module.exports = function (inventory) {
    let ans = [];
    if(inventory == null || inventory.length === 0)
     return ans;
    
    const i = inventory.length -1;
    ans = { "car_make" : inventory[i].car_make , "car_model" : inventory[i].car_model};
    return ans;
}