module.exports = function (inventory , n) {
    let ans = [];
     if(inventory == null || n == null || inventory.length === 0) 
        return ans;
    
    for(let i = 0; i < inventory.length; i++)
    {
        if(inventory[i].id == n){   
            ans = inventory[i];
            return ans;
        }
    }
}